# Design Pattern
## Etat
>Le design pattern état permet de changer le comportement d'un objet en fonction de son état interne. Il permet de déléguer le comportement à un objet qui représente l'état courant de l'objet. Cela permet de simplifier le code et de rendre l'objet plus facilement extensible.