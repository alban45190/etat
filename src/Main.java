public class Main {
    public static void main(String[] args) {
        Porte porte = new Porte();
        porte.ouvrirPorte();
        porte.fermerPorte();
        porte.ouvrirPorte();
        porte.ouvrirPorte();
    }
}