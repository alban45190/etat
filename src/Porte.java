import states.PorteEstFermee;
import states.State;

public class Porte {

    private State state;

    private boolean estOuverte;

    public Porte() {
        this.state = new PorteEstFermee();
        this.estOuverte = false;
    }

    public void ouvrirPorte() {
        try {
            this.state = this.state.ouvrir();
            this.estOuverte = true;
            System.out.println("La porte est ouverte");
        } catch(UnsupportedOperationException e) {
            System.out.println("La porte est déjà ouverte");
        }
    }
    public void fermerPorte() {
        try {
            this.state = this.state.fermer();
            this.estOuverte = false;
            System.out.println("La porte est fermée");
        } catch(UnsupportedOperationException e) {
            System.out.println("La porte est déjà fermée");
        }
    }


}
