package states;

public class PorteEstOuverte implements State {

    @Override
    public State ouvrir() {
        throw new UnsupportedOperationException();
    }

    @Override
    public State fermer() {
        return new PorteEstFermee();
    }
}
