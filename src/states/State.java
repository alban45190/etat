package states;

public interface State {

    State ouvrir() throws UnsupportedOperationException;
    State fermer() throws UnsupportedOperationException;

}
