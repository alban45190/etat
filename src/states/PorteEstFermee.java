package states;

public class PorteEstFermee implements State {

    @Override
    public State ouvrir() throws UnsupportedOperationException {
        return new PorteEstOuverte();
    }

    @Override
    public State fermer() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
